#!/usr/bin/env bash

REPO=ubikit
REPO_URL=https://ucic-1@bitbucket.org/ucic-1/$REPO.git

echo "Starting UBI Kit install"
curl -s 'http://ubikit.ucic.io/installing' > /dev/null

cd ~
echo "Downloading..."
sudo apt-get -qq install git # git doesn't come included in raspbian lite
git clone --depth 1 $REPO_URL
echo "Copying build to home directory"
mv $REPO/build/* ~
echo "Cleaning up..."
rm -rf $REPO
echo "Installing..."
bash setup-ubi.sh

curl -s 'http://ubikit.ucic.io/success-installing' > /dev/null
