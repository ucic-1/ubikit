# Install
`curl https://bitbucket.org/ucic-1/ubikit/raw/master/install.sh | sh`
That's it!

#### To enable DUET configuration
`sed -i "/^enabled\.cirruslogic=/s/=.*/=true/" ~/ucic-rpi.properties && sudo reboot now`
