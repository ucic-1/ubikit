#!/usr/bin/env bash

# Should be raspberrypi
PROFILE=$(uname -n)
HOME_DIR=$HOME
install_nodejs=TRUE
ip_addr=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')

USER_NO_PASS_STRING="$USER ALL=(ALL) NOPASSWD: ALL"
sudo cp /etc/sudoers /etc/sudoers.backup
sudo sed -i "/$USER_NO_PASS_STRING/d" /etc/sudoers
echo "$USER_NO_PASS_STRING" | sudo tee -a /etc/sudoers 1> /dev/null

##################################
# autostart ssh (since Raspbian Jessie)
##################################
sudo service ssh start

# Remove update since new raspbian build to speed up install process
# sudo apt-get -qq update

# Make sure expect is installed
sudo apt-get -qq --yes --force-yes install expect &> /dev/null

#install requirements for the UCIC console
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - 1> /dev/null
sudo apt-get -qq install -y nodejs

# make sure java is installed
sudo apt-get -qq install oracle-java8-jdk

# Make sure VLC is installed
sudo apt-get -qq --yes --force-yes install vlc

# Make sure forever is installed
sudo npm install -g forever &> /dev/null

if [ "$PROFILE" == "raspberrypi" ]; then
  # Install pi4j
  if [ ! -d "/opt/pi4j/lib" ]; then
      curl -s get.pi4j.com | sudo bash 1> /dev/null
  fi
fi

# Make sure JDK 1.8 is installed
java_8_present=$(update-alternatives --list java | awk '{if ($0 ~ /-8-/) print $0;}')
if [[ ! "$java_8_present" ]]; then
    sudo echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" > webupd8team-java.list
    sudo echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" >> webupd8team-java.list
    sudo mv webupd8team-java.list /etc/apt/sources.list.d/
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
    sudo echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
    sudo echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
    sudo apt-get -qq install -y oracle-java8-installer
fi

# Cycle through update-alternatives --config java options until 1.8 is set
set_index=1
while [[ "$_java" ]]
do
    # Check current java version
    # version=$(java -version 2>&1 | sed 's/java version "\(.*\)\.\(.*\)\..*"/\1\2/; 1q')
    version=$(java -version 2>&1 | head -n 1 | awk -F '"' '{print $2}')
    echo version "$version"
    # if [ "$version" -eq "18" ]; then
    if [[ $version == 1.8* ]]; then
        break
    fi

    # Switch java version
    expect -c "
       set timeout 1
       spawn sudo update-alternatives --config java
       expect {
        timeout {exit 1}
        eof {exit 1}
        \"type selection number: \" { send $set_index\r }
       }
       sleep 1
       exit
    "
    set_index=$(($set_index + 1))
done

if [ "$PROFILE" == "raspberrypi" ]; then
  # Make sure hostapd is installed
  sudo apt-get -qq --yes --force-yes install hostapd
  sudo update-rc.d hostapd remove

  # Make sure dnsmasq is installed
  sudo apt-get -qq --yes --force-yes install dnsmasq
  sudo update-rc.d dnsmasq remove

  sudo chmod 666 /etc/wpa_supplicant/wpa_supplicant.conf
fi

# setup:
#	Unzip ubi-avs.tar.gz, and copy property file into home

cd $HOME_DIR
ucic_folder=$HOME_DIR/ucic

sudo rm -rf $ucic_folder
mkdir $ucic_folder
tar zxf ubi-kit.tar.gz -C $ucic_folder --strip-components=1
rm -rf ubi-kit.tar.gz

if [ "$PROFILE" == "raspberrypi" ]; then
  cd $ucic_folder
  sudo mv node-app/config/hostapd.conf /etc/hostapd/hostapd.conf
  sudo sed -i '/.*DAEMON_CONF=.*/d' /etc/default/hostapd
  echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' | sudo tee -a /etc/default/hostapd 1> /dev/null
  sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.old &>/dev/null
  cd $ucic_folder
  sudo mv node-app/config/dnsmasq.conf /etc/dnsmasq.conf
  #add line to hosts file for the WiFi settings
  echo "172.24.1.1      raspberrypi" | sudo tee --append /etc/hosts 1>/dev/null
  echo "127.0.0.1       raspberrypi.ucic.io" | sudo tee --append /etc/hosts 1>/dev/null
fi

if [ "$PROFILE" == "udoo" ]; then
  cd $ucic_folder
  sudo cat /etc/pulse/default.pa | awk '{if ($0 ~ /load-module module-udev-detect/) print "load-module module-udev-detect tsched=0"; else print $0; }' >> $ucic_folder/temp.file
  sudo mv /etc/pulse/default.pa /etc/pulse/default.pa.backup
  sudo mv $ucic_folder/temp.file /etc/pulse/default.pa
  sudo cat /etc/pulse/daemon.conf | awk '{if ($0 ~ /default-fragments =/) print "default-fragments = 5"; else if ($0 ~ /default-fragment-size-msec =/) print "default-fragment-size-msec = 2"; else print $0; }' >> $ucic_folder/temp.file
  sudo mv /etc/pulse/daemon.conf /etc/pulse/daemon.conf.backup
  sudo mv $ucic_folder/temp.file /etc/pulse/daemon.conf
  pulseaudio --kill
fi

cd $HOME_DIR
sudo mv ucic-rpi.properties ucic-rpi.properties.old &>/dev/null
sudo mv $ucic_folder/ucic-rpi.properties .
sudo sed -i '/.environment.profile=.*/d' ucic-rpi.properties
echo $'\n' | sudo tee -a ucic-rpi.properties 1> /dev/null
echo "environment.profile=$PROFILE" | sudo tee -a ucic-rpi.properties 1> /dev/null

cd $ucic_folder

cat avs-java | awk -v HOME_DIR_VAR="HOME_DIR=\"$HOME_DIR\"" '{if ($0 ~ /HOME_DIR=/) print HOME_DIR_VAR; else print $0; }' >> avs-java.temp
mv avs-java.temp avs-java
cat avs-java | awk -v USER_VAR="EXEC_USER=\"$USER\"" '{if ($0 ~ /EXEC_USER=/) print USER_VAR; else print $0; }' >> avs-java.temp
mv avs-java.temp avs-java

cat avs-login | awk -v HOME_DIR_VAR="HOME_DIR=\"$HOME_DIR\"" '{if ($0 ~ /HOME_DIR=/) print HOME_DIR_VAR; else print $0; }' >> avs-login.temp
mv avs-login.temp avs-login
cat avs-login | awk -v USER_VAR="EXEC_USER=\"$USER\"" '{if ($0 ~ /EXEC_USER=/) print USER_VAR; else print $0; }' >> avs-login.temp
mv avs-login.temp avs-login

cd $ucic_folder/goog-app/bin
cat goog-asst | awk -v HOME_DIR_VAR="HOME_DIR=\"$HOME_DIR\"" '{if ($0 ~ /HOME_DIR=/) print HOME_DIR_VAR; else print $0; }' >> goog-asst.temp
mv goog-asst.temp goog-asst
cat goog-asst | awk -v USER_VAR="EXEC_USER=\"$USER\"" '{if ($0 ~ /EXEC_USER=/) print USER_VAR; else print $0; }' >> goog-asst.temp
mv goog-asst.temp goog-asst

cd $ucic_folder
sudo chmod 755 avs-java
sudo chown root:root avs-java
sudo cp avs-java /etc/init.d/

cd $ucic_folder
sudo chmod 755 avs-login
sudo chown root:root avs-login
sudo cp avs-login /etc/init.d/

cd $ucic_folder/goog-app/bin
sudo chmod 755 goog-asst
sudo chown root:root goog-asst
sudo cp goog-asst /etc/init.d/

##################################
# set up .asoundrc
##################################

if [ ! -f $HOME_DIR/.asoundrc ]; then
    echo "Creating default file at $HOME_DIR/.asoundrc"
    touch $HOME_DIR/.asoundrc
    echo "pcm.!default {" | sudo tee --append $HOME_DIR/.asoundrc
    echo "    type asym" | sudo tee --append $HOME_DIR/.asoundrc
    echo "    playback.pcm \"plughw:0\"" | sudo tee --append $HOME_DIR/.asoundrc
    echo "    capture.pcm \"plughw:1\"" | sudo tee --append $HOME_DIR/.asoundrc
    echo "}" | sudo tee --append $HOME_DIR/.asoundrc
fi

sudo update-rc.d avs-login defaults
sudo update-rc.d avs-java defaults

#optionally reload /etc/init.d services
echo "Reloading daemons."
sudo systemctl daemon-reload

sudo service avs-login restart

echo "============================================================"
echo "If you are operating a headless setup, please update your"
echo "hosts file on any machine you use to map your pi's ip address"
echo "to raspberrypi.ucic.io. (i.e. add the following line to the "
echo "bottom of the /etc/hosts file)"
echo ""
echo "$ip_addr      raspberrypi.ucic.io"
echo ""
echo "Then, proceed to https://raspberrypi.ucic.io:3000"
echo "============================================================"
